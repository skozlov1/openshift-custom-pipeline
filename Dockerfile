FROM nodejs:10

WORKDIR /usr/src/app

COPY . /usr/src/app

ENV PORT 5000
EXPOSE 5000
CMD [ "node", "/usr/src/app/bin/www" ]
